package QueensCosulschiSwing;

import javax.swing.*;

public class App {
    public static void main( String[] args )    {

        JFrame frame = new JFrame("My First GUI");

        CreateJPanelWithJSpinner createJPanelWithJSpinner = new CreateJPanelWithJSpinner();
        JPanel panel1 = createJPanelWithJSpinner.doContinut();
        String n = panel1.getComponent(2).getName();

        JPanel jPanel2 = Panel2.createPanel(1);

        JSplitPane principal = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, panel1, jPanel2);
        frame.getContentPane().add(principal);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(900, 600);
        frame.setVisible(true);
    }
}
