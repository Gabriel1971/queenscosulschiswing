package QueensCosulschiSwing;

import javax.swing.*;

public class Panel2 {
    static JPanel jPanel = new JPanel();

    static JPanel createPanel(int n) {
        jPanel.setBounds(250, 10, 500, 400);
        JLabel labelQ[][] = new JLabel[n][n];
        int[] VS = FindSolution.findSolution(n);
        jPanel.setLayout(/*new GridLayout(n, n, n, n)*/null);
        int i, j;
        for (i = 0; i < n; i++)
            for (j = 0; j < n; j++) {
                labelQ[i][j] = new JLabel();
                labelQ[i][j].setBounds(30*(i+1), 25*(j+1), 25,20);

                if(VS[i] == j+1){
                    labelQ[i][j].setText("Q!"); // matrix[i][j] = 1;
                } else {
                    labelQ[i][j].setText("*"); // matrix[i][j] = 0;
                }
                if (n == 1) {
                    labelQ[i][j].setVisible(false);
                }else {
                    labelQ[i][j].setVisible(true);
                }
                jPanel.add(labelQ[i][j]);
            }
        jPanel.setVisible(true);
        return jPanel;
    }
}
