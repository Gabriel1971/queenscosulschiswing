package QueensCosulschiSwing;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CreateJPanelWithJSpinner implements ChangeListener {
    static int n;
    static JSpinner spinner;
    static JLabel label;
    static JLabel label1;
    static JButton button;

    CreateJPanelWithJSpinner() {
    }

    protected JPanel doContinut() {

        JPanel continut = new JPanel();
        continut.setLayout(new FlowLayout());//le pune in linie...
        //continut.setLayout(null); //tine cont de setBounds
        continut.setBounds(10, 10, 500, 350);

        CreateJPanelWithJSpinner createJPanelWithJSpinner1 = new CreateJPanelWithJSpinner();

        SpinnerNumberModel spinnerNumberModel = new SpinnerNumberModel(5, 4, 20, 1);
        spinner = new JSpinner(spinnerNumberModel);
        spinner.setBounds(10, 10, 40, 40);
        spinner.addChangeListener(createJPanelWithJSpinner1);
        continut.add(spinner, BorderLayout.WEST);

        label = new JLabel();
        label.setBounds(100, 200, 60, 20);
        label.setBorder(BorderFactory.createBevelBorder(2));
        continut.add(label);

        button = new JButton("Ok n?");
        button.setBounds(20, 230, 70, 20);
        button.addActionListener(new ButtonAction());
        continut.add(button);

        label1 = new JLabel();
        label1.setBounds(100, 240, 60, 20);
        label1.setBorder(BorderFactory.createBevelBorder(2));
        continut.add(label1);

        return continut;
    }

    public void stateChanged(ChangeEvent event) {
        int m = (int) spinner.getValue();
        label.setText("m = " + m);
    }
}

