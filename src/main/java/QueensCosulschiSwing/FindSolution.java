package QueensCosulschiSwing;
/**COD SCRIS PE BAZA PSEUDOCOD CARTE MIREL COSULSCHI - ALGORITMI FUNDAMENTALI*/
public class FindSolution {

    static int[] findSolution(int n) {
        int[] VS = new int[n];
        int i = 0;

        while (i >= 0) {
            boolean gasit = false;
            while ((VS[i] < n) && (gasit != true)) {
                VS[i]++;
                if (Test.test(VS, i) == true) {
                    gasit = true;
                }
            }
            if (gasit == true) {
                if (i == n - 1) {
                    return VS;
                } else {
                    i++;
                    VS[i] = 0;
                }
            } else {
                i--;
            }
        }
        return null;
    }
}
