package QueensCosulschiSwing;

public class Afiseaza {

    static void afis(int[][] Matrix){
        for (int i = 0; i < Matrix.length; i++) {
            System.out.println("");
            for (int j = 0; j < Matrix[i].length - 1; j++){
                System.out.print(Matrix [i][j] + "\t");
            }
        }
    }

    static int[][] matrixSolutie (int[] VS){
        int[][] matrix = new  int[VS.length][VS.length + 1];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++){
                if(VS[i] == j+1){
                    matrix[i][j] = 1;
                } else {
                    matrix[i][j] = 0;
                }
            }
        }
        return matrix;
    }
}
